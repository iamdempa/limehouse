import boto3
import botocore
from botocore.exceptions import ClientError
import logging
import os

# logging related configs 
logging.basicConfig(format='%(asctime)s,%(msecs)03d %(levelname)-8s %(message)s',level=logging.INFO)
logging.info('Program Started...\n')

# set the string that needs to match 
SEARCH_STRING = ''

# Check if the environment variable is set
if "SEARCH_STRING" in os.environ:
  value = os.getenv("SEARCH_STRING")
  if value:
    logging.info('\'SEARCH_STRING\' is SET, using the value is: %s\n', value)
    SEARCH_STRING = value
  else:
    logging.info('\'SEARCH_STRING\' is NOT Set, using the default value \'limehome\' \n')
    SEARCH_STRING = 'limehome'
else:
  logging.info('\'SEARCH_STRING\' is NOT Set, using the default value \'limehome\' \n')
  SEARCH_STRING = 'limehome'
    
# Set up a client to access S3
s3 = boto3.client('s3', aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'), aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))

list_of_buckets_and_file_names = []

def search_for_substring(bucket_name, substring):
    
    print("\n")
    logging.info("Iterating through the Objects of BUCKET '%s'", bucket_name)    

    try:

        # list all the objects in the bucket
        objects = s3.list_objects_v2(Bucket=bucket_name)

        # empty list to store the file names
        file_names = []        

        for obj in objects['Contents']:

            # Get the object key (the file name) and the object itself
            key = obj['Key']

            try:

                object = s3.get_object(Bucket=bucket_name, Key=key)

                # Check if the object is a text file (by checking the ContentType)
                if object['ContentType'] == 'text/plain':

                    # Read the contents of the text file
                    contents = object['Body'].read()

                    # check subsstring matches the content
                    if substring in contents.decode():

                        # If it matches, add the file name to the list
                        file_names.append(key)
                        logging.info("Appending the FILE NAME '%s'", key) 
                        

            # If the specified key does not exist
            except s3.exceptions.NoSuchKey:
                logging.error('Key ' + key + " not found") 

            # If the action is not valid for the current state of the object
            except s3.exceptions.InvalidObjectState:
                logging.error('Invalid Object State') 

        # logging.info('File Names found in BUCKET ' + bucket_name + ": " + str(file_names)) 
        new_content = {
            "bucket_name": bucket_name,
            "file_names": file_names
        }

        list_of_buckets_and_file_names.append(new_content)

    except s3.exceptions.NoSuchBucket:
        logging.error('Bucket ' + bucket_name + " not found")  
    except ClientError as e:
        logging.error(e)   

    return list_of_buckets_and_file_names


# Get all the buckets
buckets = s3.list_buckets()

# Iterate through the bucket names
for bucket in buckets['Buckets']:
    bucket_name = bucket["Name"]
    list_of_buckets_and_file_names = search_for_substring(bucket_name, SEARCH_STRING)
    logging.info('Program Finished...')  


print("\n-------------------------- Result --------------------------")
for result in list_of_buckets_and_file_names:
    if len(result["file_names"]) > 0 :
        print("Bucket: " + result["bucket_name"])
        for file_name in result["file_names"]:
            print("Files Found: " + file_name)
        print("\n")