import boto3
import concurrent.futures
import logging
import os
from datetime import datetime

# ask boto3 to use the AWS profile 
# boto3.setup_default_session(profile_name='jananath')

# set logger configs 
logger = logging.getLogger()
logging.basicConfig(format='%(asctime)s,%(msecs)03d %(levelname)-8s %(message)s',level=logging.INFO)

# get current working dir
cwd = os.getcwd()
cwd = os.path.dirname(os.path.abspath(__file__))
filename = cwd + "/" + str(datetime.today().strftime('%Y-%m-%d')) + "_output.log"
fh = logging.FileHandler(filename)
logger.addHandler(fh)

# Set up a client to access S3
s3 = boto3.client('s3', aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'), aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))

# function to search for the matched substring in file content 
def search_substring_in_s3_buckets(substring, bucket_names):

    # search the bucket content for the given content(substring)
    def search_in_bucket(bucket_name):

        
        logging.info('Iterating through the Objects of BUCKET %s', bucket_name)        

        # empty result to append the file names 
        result = []

        list_of_buckets_and_file_names = []

        # get a paginator to iterate over an entire result (when object count is > 1000)
        paginator = s3.get_paginator('list_objects_v2')

        for page in paginator.paginate(Bucket=bucket_name):

            for obj in page['Contents']:

                # Get the object key (the file name) and the object itself
                key = obj['Key']
                try:

                    getObj = s3.get_object(Bucket=bucket_name, Key=key)

                    # check if content type matches the text
                    if getObj['ContentType'] == 'text/plain':
                        content = getObj['Body'].read()

                        # Convert the contents to a string and check if it contains the substring
                        if substring in content.decode():

                            # If it does, add the file name to the list
                            logging.info('Appending the FILE NAME ' + key) 
                            result.append(key)

                # If the specified key does not exist
                except s3.exceptions.NoSuchKey:
                    logging.error('Key ' + key + " not found") 

                # If the action is not valid for the current state of the object
                except s3.exceptions.InvalidObjectState:
                    logging.error('Invalid Object State') 

                new_content = {
                    "bucket_name": bucket_name,
                    "file_names": result
                }

            list_of_buckets_and_file_names.append(new_content)
        return list_of_buckets_and_file_names

    # asynchronously executing callables
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = [executor.submit(search_in_bucket, bucket) for bucket in bucket_names]
        return [r.result() for r in concurrent.futures.as_completed(results)]


response = s3.list_buckets()
bucket_names = [bucket['Name'] for bucket in response['Buckets']]

logging.info('Program Started...\n')


# set the string that needs to match 
SEARCH_STRING = ''

# Check if the environment variable is set
if "SEARCH_STRING" in os.environ:
  value = os.getenv("SEARCH_STRING")
  if value:
    logging.info('\'SEARCH_STRING\' is SET, using the value is: %s\n', value)
    SEARCH_STRING = value
  else:
    logging.info('\'SEARCH_STRING\' is NOT Set, using the default value \'limehome\' \n')
    SEARCH_STRING = 'limehome'
else:
  logging.info('\'SEARCH_STRING\' is NOT Set, using the default value \'limehome\' \n')
  SEARCH_STRING = 'limehome'


results = search_substring_in_s3_buckets(SEARCH_STRING, bucket_names)

logging.info('Program Finished...')  

print("\n-------------------------- Result --------------------------")

# [print(f"BUCKET: {file['bucket_name']}\nFILES: {file['file_names']}\n") if len(file['file_names']) > 0 else print(f"BUCKET: {file['bucket_name']}\n") for result in results for file in result]

for result in results:
    for file in result:
        if len(file['file_names']) > 0:
            print(f"BUCKET: {file['bucket_name']}")
            print(f"FILES FOUND: {file['file_names']}\n")
