IMAGE_NAME = find_in_s3
AWS_ACCESS_KEY_ID = $(shell echo $$AWS_ACCESS_KEY_ID)
AWS_SECRET_ACCESS_KEY = $(shell echo $$AWS_SECRET_ACCESS_KEY)
SEARCH_STRING = $(shell echo $$SEARCH_STRING)

run:
	python3 app/prod.py 
build-app:
	docker build -t $(IMAGE_NAME) app/
run-app:
	docker run --rm -e AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) \
	-e AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) \
	-e SEARCH_STRING=$(SEARCH_STRING) $(IMAGE_NAME)