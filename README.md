# LimeHouse

This repository containes a simple `python` based application to retrieve the names of files that matches a given substring in their content. The result will outout to the terminal which contains **all Bucket names with file names, where the file content contains said substring.** and the log file (`output.log`) is generated for troubleshooting 

> Assumptions Made - The files are `plain text` files and are of type `.txt`

## Directory Hierarchy

This repository has the following directory hierarchy. The `app/` directory contains the script `prod.py` responsible for the expected output.

```
.
├── Makefile
├── README.md
├── app
│   ├── 2022-12-20_output.log
│   ├── Dockerfile
│   ├── dev.py
│   ├── prod.py
│   └── requirements.txt
└── samples
    ├── . . . 
```

`app/` - directory contains the source codes for running the `python` application. This contains 2 files namely `dev.py` and `prod.py`. The both files are responsible for delivering the same output, but to make things simple and clearer, as name suggests, 

- `dev.py` - simple and can be used for daily basis to see for any matching substrings. 
- `prod.py` - simple but use asynchrounous calls to the S3 in case the bucket sizes and exponentially larger and have millions of objects stored. And the this script is instrumented to run in a production environment. 

- `Dockerfile` - created for making the lives of the developers easier. application can easyli shareable and run anywhere where there is `Docker` installed. 

- `requirements.txt` - associated with the Dockerfile to install python packages

- `<DATE>-output.log` - As an extention to this application, a log file is created for later use (eg: troubleshooting purposes)

`sample/` - contains some sample text files that can be uploaded to S3 to test these scripts

## Prerequisites 

1. Since this simple script talks to `AWS` API's, you need to have an;

    - `AWS_ACCESS_KEY_ID`
    - `AWS_SECRET_ACCESS_KEY`

2. [Python](https://www.python.org/downloads/) - `3.7` or higher

or Just

3. [Docker](https://docs.docker.com/get-docker/)

## What it does?

This simple python application will iterate over all `S3` buckets and retrive all the file names that matches the particular substring in their content.

## Run your Application

> Prerequisites - In order to run the application, you need to have an `AWS_ACCESS_KEY_ID` and an `AWS_SECRET_ACCESS_KEY`. Export the values as below.


```
// set authentication keys, region
export AWS_ACCESS_KEY_ID=<YOUR-AWS_ACCESS_KEY_ID>

export AWS_SECRET_ACCESS_KEY=<YOUR-AWS_SECRET_ACCESS_KEY>

export AWS_DEFAULT_REGION=<YOUR-AWS_DEFAULT_REGION>
```

And by default, the `SEARCH_STRING` (which is the substring) value is set to `limehome`. But if you need to search for something else, set the value of `SEARCH_STRING`

```
export SEARCH_STRING=some-other-value
```

## 1. Usual way

You can simply run the application with a single command;

```
// then
make run
```

## 2. Get the most out of Docker

This is the easiest and most convenient way to run this application. Make sure you have `Docker` installed as stated in the [Prerequisites](##Prerequisites)

```
// build the docker image
make build-app

// run the docker image
make run-app
```


# Concepts

### Q.) Infrastructure as Code


**What is IaC?**

Infrastructure as code (IaC) is a process to manage and provision your infrastructure using (written) code, rather than manually configuring resources through a user interface (such as a console of a public cloud provider). This can include things like servers, networking components, and software configurations.

**Usability**

- Version control ability : Possibility to trackthe changes to infrastructure over time and roll back to previous versions if it is necessary.

- Collaboration: Changes to the infrastructure can be made concurrently by multiple team members, and then reviewed and approved prior to implementation.

- Automation: You can automate the provisioning and management of the infrastructure, which can save time and reduce the risk of any possible human errors.


**Alternatives?**

There are several alternatives to using IaC. This includes manually configuring the infrastructure through a user interface (such as Console) or using a configuration management tool like Chef or Ansible. However, these alternatives may not offer the same level of version control, Usability and collaboration as IaC.


### Q.) Observability


**What is Observability?**

This is simply the ability to monitor and understand the behavior of a system, which is made up of multiple components that communicate with each other to perform various tasks. It is important to have good observability because it helps identify issues, optimize performance, and facilitate debugging and troubleshooting.


**What do we Want to Observe?**

Basically we want to observe various metrics and logs of the applications, as well as trace requests as they flow through the system. This involves collecting data from the different components of the system, storing it in a central location, and visualizing it in a way that is easy to understand.

**Challenges and How to Overcome?**

In an era of microservices architecture, one big challenge in a distributed environment is that it can be difficult to understand the overall funcationality and behaviour of the system by looking at individual components. It is also a challenge to trace requests as they flow through the system, especially if there are multiple layers of microservices involed. 

To solve this challenge, we can use tools and techniques such as monitoring, tracing, visualization, and alerting to get a better understanding of the system as a whole.

### Security

If I were to join the team and put in charge of the Security of the LimeHome's AWS infrastrcuture, I would mainly focus on the following 3 areas to make sure to avoid any risks accosiated with breaches.

- `IAM` - Make sure we have defined appropriate IAM policies and roles and are in place. Such as using MFA authentication for all administrative accounts and setup password policies that requires strong and unique passwords. And also that each User groups have the `least-privilege` permissions to perform different tasks. These mitigations ensure that only authorized users can access your resources

- `Network` - Make sure all the network component layers are properly configured. This can be validated by checking if appropriate security groups and network ACLs are in place and they are only exposing only the required inbound/outbound rules. And for different environments, it is always a best practice t implement proper network segmentation to limit access to sensitive resources. These controls help protect against unauthorized access to our resources.

- `Monitoring & Logging` - Since almost all the API requests are associated with different AWS services, it is always a better idea to enable `AWS CloudTrail` to monitor and log all API activity. This feature make sure it logs every API request and later it is very useful to track who made what. And also I will setup `Amazon CloudWatch` to monitor resources and implement different alerts based on different conditions such that when certain thresholds or conditions are met, engineers will be informed right away. And also I will implement a log retention policy for different applications to retain logs for a sufficient period of time. These mitigations steps helps detect and respond to any potential threats or issues.
